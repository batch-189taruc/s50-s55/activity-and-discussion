import {Fragment, useEffect, useState} from 'react';
// import coursesData from '../data/coursesData';
import Courses from '../components/Courses';
export default function CoursesH() {
	/*console.log(coursesData)
	console.log(coursesData[0])*/
	const [courses, setCourses] = useState([])

	useEffect(() => {
		fetch('http://localhost:4000/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course =>{
				return (
					<Courses key={course._id} courseProp={course}/>
					)

			}))
		})


	}, [])
	/*const coursee = coursesData.map(course =>{
		return(
				<Courses key={course.id} courseProp={course}/>

			)
	})*/
	return(

			
			<Fragment>
				<h1>Courses</h1>
				{courses}
			</Fragment>
		)


}