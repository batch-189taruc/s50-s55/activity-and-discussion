import {useState, useEffect, useContext } from 'react'; //s54 activity
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {
	//State hooks to store the values of the input fields
	const navigate = useNavigate();
	const {user} = useContext(UserContext)//s54 activity
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	//State to determine whether the submitbtn is enabled or not
	const [isActive, setIsActive] = useState(false);
	function registerUser(e){
		
		e.preventDefault()

	fetch("http://localhost:4000/users/checkEmail", {
			method: "POST",
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true){
				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})
			} else{
				fetch("http://localhost:4000/users/register", {
					method: "POST",
					headers: {
						'Content-type': 'application/json'
					},
					body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNo,
					password: password1
					})

				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if (data === true){
						Swal.fire({
							title: "Success!",
							icon: "success",
							text: "Welcome to Zuitt"
						})
						setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");
						navigate("/login");
					}else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Try again!"
						})
					}
				})
			}
		})

		setFirstName("");
		setLastName("");
		setEmail("");
		setMobileNo("");
		setPassword1("");
		setPassword2("");
	};
	useEffect(() => {
	if((firstName !== "" && lastName !== "" && mobileNo.length===11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
		setIsActive(true)
	}else{
		setIsActive(false)
	}

	}, [firstName, lastName, mobileNo, email, password1, password2]);

	
	return(
		(user.id !== null) ? //s54 activity
		<Navigate to="/courses"/>
		:
		<Form onSubmit={(e)=> registerUser(e)}>
			<h1 className="text-center">Register</h1>
			 <Form.Group className="mb-3" controlId="firstName">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
		        		type="text" 
		        		placeholder="Enter First Name"
		        		value={firstName}
		        		onChange={e => {setFirstName(e.target.value)}}
		        		required />
		      </Form.Group>
		       <Form.Group className="mb-3" controlId="lastName">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
		        		type="text" 
		        		placeholder="Enter Last Name"
		        		value={lastName}
		        		onChange={e => {setLastName(e.target.value)}}
		        		required />
		      </Form.Group>
		      <Form.Group className="mb-3" controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        		type="email" 
		        		placeholder="Enter email"
		        		value={email}
		        		onChange={e => {setEmail(e.target.value)}}
		        		required />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>
		       <Form.Group className="mb-3" controlId="mobileNo">
		        <Form.Label>Mobile Number</Form.Label>
		        <Form.Control 
		        		type="text" 
		        		placeholder="Enter First Name"
		        		value={mobileNo}
		        		onChange={e => {setMobileNo(e.target.value)}}
		        		required />
		      </Form.Group>
		      <Form.Group className="mb-3" controlId="password1">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        		type="password" 
		        		placeholder="Password" 
		        		value={password1}
		        		onChange={e => {setPassword1(e.target.value)}}
		        		required />
		      </Form.Group>

		      <Form.Group className="mb-3" controlId="password2">
		        <Form.Label>Verify Password</Form.Label>
		        <Form.Control 
		        		type="password" 
		        		placeholder="Password"
		        		value={password2} 
		        		onChange={e => {setPassword2(e.target.value)}}
		        		required />
		      </Form.Group>
		      
		      {/*<Button variant="primary" type="submit" id="submitBtn">
		        Submit
		      </Button>*/}

		      {
		      	isActive ?
		      		<Button variant="primary" type="submit" id="submitBtn">
		        	Submit
		      		</Button>:
		      		<Button variant="danger" type="submit" id="submitBtn" disabled>
		        	Submit
		      		</Button>
		      }
		    </Form>



		)

}