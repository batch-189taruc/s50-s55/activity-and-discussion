import React from 'react';


//Creat a Context Object
//a context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the app.
//Context object is a different approach to passing information between components and allows easier acces by avoiding the use of prop-drilling
const UserContext = React.createContext()

export const UserProvider = UserContext.Provider;

export default UserContext;