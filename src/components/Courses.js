//import { useState, useEffect } from 'react';
import {Card, Button} from 'react-bootstrap'
import { Link } from 'react-router-dom'

//deconstructed
export default function Courses({courseProp}){
	
	
	
	/*
	Use the state hook for this component to be able to store its state. States are used to keep track of information related to individual components.

	Syntax:
		const [getter, setter] = useState(initialGetterValue)

	*/
// my Activity
/*	const[count, setCount] = useState(0)

	function enroll(){
		
		if (count < 30){
			setCount(count+1)
		}
	}
	const[seat, taken] = useState(30)
	function seats(){
		if (seat < 1){
			alert("No more seats")
			
		} else{
			taken(seat-1)
		}
	}
	onClick={()=>{enroll();seats()}}
*/
/*const[count, setCount] = useState(0)
const[seat, taken] = useState(30)

function enroll (){
	setCount(count +1);
	console.log('Enrollees: ' + count)
	taken(seat - 1)
	console.log('Seats: ' + seat)
}

useEffect(() => {
	if (seat ===0) {
		alert('No more seats available')
	}
}, [seat])*/

	//deconstructed
	const {name,description,price, _id} = courseProp;
	console.log(courseProp)
	return(
			
				
					<Card>
					<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description</Card.Subtitle>
					<Card.Text>
					{description}
					</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text> Php  {price}
					</Card.Text>
					
					<Button variant="primary" as={Link} to ={`/courses/${_id}`}>Details</Button>
				{/*<Link className="btn btn-primary" to="/courseview">Details</Link> this works the same as above*/}
					</Card.Body>
					</Card>
					
			

		)

}