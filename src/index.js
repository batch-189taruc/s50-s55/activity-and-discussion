import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
//import AppNavbar from './AppNavbar'
//Import bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/*const name = "Sherlock Holmes";
const student = {
  firstName: 'Nehemiah',
  lastName: 'Ellorico'
}

function userName(user){
  return user.firstName + ' ' + user.lastName
}
const element = <h1> Hello, {userName(student)} </h1>

root.render(element)*/